var nodemailer = require('nodemailer');

// create reusable transporter object using SMTP transport
var transporter = nodemailer.createTransport({
  service: 'Gmail',
  secure: true,
  port: 80, //465  //the code is the same, just port: 587/465 rather than 80
  auth: {
    user: process.env.email,
    pass: process.env.pass
  }
});
/*
var mailOptions = {
  //from: 'rathnaprakash86@gmail.com', // sender address
  to: 'rathnaprakash86@gmail.com', // list of receivers
  subject: 'Mail From Website', // Subject line
  html: 'Your one time password is : <b>' + 'Hello World' + ' </b>' // html body
}*/

var sendMail = function(mailOptions) {
  let status={};
  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      console.log(error);
      status= error;
    } else {
      console.log('Message %s sent: %s', info.messageId, info.response,JSON.stringify(info));
      status= info;
    }
  });
  return status;
}

module.exports.sendMail = sendMail;
