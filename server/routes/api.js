const express = require('express');
const cors = require('cors')
const router = express.Router();
const emailService = require('./emailService');



/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

router.post('/emailService', (req, res) => {
  if (!req.body) return res.sendStatus(400)

  let mailOptions = {};
  mailOptions.from = req.body.fromemail;
  mailOptions.to = req.body.toemail;
  mailOptions.subject = req.body.subject;
  mailOptions.html = req.body.message;
  let status = emailService.sendMail(mailOptions);
  res.json(JSON.stringify(status))
});





module.exports = router;
