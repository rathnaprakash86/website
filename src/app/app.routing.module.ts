import { Routes, RouterModule }   from '@angular/router';
import {NgModule} from '@angular/core';


import { HomeComponent } from './home/home.component';
import { AboutmeComponent } from './aboutme/aboutme.component';
import { SkillsComponent } from './skills/skills.component';
import { PortfoliaComponent } from './portfolia/portfolia.component';
import { ContactComponent } from './contact/contact.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';


const navRoutes: Routes = [{ path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'aboutme', component: AboutmeComponent },
  { path: 'techskill', component: SkillsComponent },
  { path: 'portfolio', component: PortfoliaComponent },
  { path: 'contact', component: ContactComponent },
  { path: '**', component: PagenotfoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(navRoutes)
  ], exports: [RouterModule]
})
export class AppRoutingModule {

}
