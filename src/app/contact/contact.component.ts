import { ElementRef, Renderer2, Component, OnInit, } from '@angular/core';
import { Response } from '@angular/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {EmailService} from './emailService';


@Component({
  selector: 'app-prp-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],

})
export class ContactComponent implements OnInit {
  contactForm: FormGroup;
  lat: Number = 40.592692;
  lng: Number = -111.957498;
  mycaptchaResponse: string;
  constructor(private emailService: EmailService,
    private el: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    this.contactForm = new FormGroup({
      'senderName': new FormControl(null, Validators.required),
      'fromemail': new FormControl(null, [Validators.required, Validators.email]),
      'message': new FormControl(null, Validators.required)
    });
    window.scrollTo(0, 0);
  }
/*
  onSendMessage() {
    let sendStatus: string = '';
    // console.log(this.contactForm.valid);
    if (this.contactForm.valid) {
      // console.log(this.contactForm.value);
      this.emailService.sendEmail(this.getMessageOptions(this.contactForm.value, 'contact')).subscribe();
      this.emailService.sendEmail(this.getMessageOptions(this.contactForm.value, 'thanks')).subscribe();
      this.contactForm.reset();

      this.flashMessagesService.show('Thanks for reaching out!', {
        classes: ['alert', 'alert-success'], // You can pass as many classes as you need
        timeout: 5000, // Default is 3000
      });

    } else {
      this.flashMessagesService.show('Please fill out the form !', {
        classes: ['alert-danger'], // You can pass as many classes as you need
        timeout: 2000, // Default is 3000
      });
    }
  }

  public submit(captchaResponse: string): void {
    this.onSendMessage();
  }
  getMessageOptions(data: any, type: string) {
    let mailOptions = {
      'fromemail': '',
      'senderName': '',
      'message': '',
      'toemail': '',
      'subject': ''
    };

    if (type === "contact") {
      mailOptions.fromemail = data.fromemail;
      mailOptions.senderName = data.senderName;
      mailOptions.message = data.message;
      mailOptions.toemail = 'rathnaprakash86@gmail.com';
      mailOptions.subject = 'MESSAGE FROM MYSITE'
    } else if (type === "thanks") {
      mailOptions.fromemail = 'rathnaprakashparameshkumar@gmail.com'
      mailOptions.senderName = data.senderName;
      mailOptions.message = 'Thanks a Lot';
      mailOptions.toemail = data.fromemail;
      mailOptions.subject = 'Thanks for Reaching rathnaprakashparameshkumar'
    }
    return mailOptions;
  }*/
}
