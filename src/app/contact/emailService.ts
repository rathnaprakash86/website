import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class EmailService {

  constructor(private http: Http) { }

  sendEmail(mailOptions: { 'fromemail': string, 'senderName': string, 'message': string, 'toemail': string, 'subject': string }) {
    const myheaders = new Headers({ 'Content-Type': 'application/json' })
    return this.http.post('./api/emailService/', mailOptions, { headers: myheaders});
  }

}
