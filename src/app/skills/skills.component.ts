import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prp-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
       window.scrollTo(0, 0);
  }

}
