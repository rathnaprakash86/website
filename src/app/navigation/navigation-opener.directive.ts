import { Directive, Renderer2, OnInit, ElementRef, HostListener} from '@angular/core';
 // Used to toggle the menu on small screens when clicking on the menu button
@Directive({
  selector: '[appNavigationOpener]'
})
export class NavigationOpenerDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) {
  }
  @HostListener('click') clicking (eventData: Event) {
    // this.renderer.setStyle(this.el.nativeElement, 'backgroundColor', 'red');
    const x = document.getElementById('navDemo');
    if (x.className.indexOf('prp-show') === -1) {
      x.className += ' prp-show';
    } else {
      x.className = x.className.replace(' prp-show', '');
    }
  }

}
