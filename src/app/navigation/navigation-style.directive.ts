import { Directive, Renderer2, OnInit, ElementRef,HostListener} from '@angular/core';

@Directive({
  selector: '[appNavigationStyle]'
})
export class NavigationStyleDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) {
  }

    @HostListener('mouseenter') mouseenter(eventData: Event) {
    // this.renderer.setStyle(this.el.nativeElement, 'backgroundColor', 'red');
    this.renderer.addClass(this.el.nativeElement, 'prp-show');
    }
    @HostListener('mouseleave') mouseleave(eventData: Event) {
    this.renderer.removeClass(this.el.nativeElement, 'prp-show');
    }

}
