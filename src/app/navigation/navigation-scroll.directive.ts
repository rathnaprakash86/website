import { Directive, Renderer2, OnInit, ElementRef, HostListener, style} from '@angular/core';
import { ValueTransformer } from '@angular/compiler/src/util';

@Directive({
  selector: '[appNavigationscroll]'
})
export class NavigationscrollDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) {
  }

  @HostListener('window:scroll', ['$event']) onScrollEvent($event: any) {
  if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
    this.renderer.addClass(this.el.nativeElement, 'prp-bar');
     this.renderer.addClass(this.el.nativeElement, 'prp-card-2');
     this.renderer.addClass(this.el.nativeElement, 'prp-animate-top');
      this.renderer.addClass(this.el.nativeElement, 'prp-white');
     // this.renderer.setStyle(this.el.nativeElement, 'color', 'black');
    } else {
      this.renderer.removeClass(this.el.nativeElement, 'prp-card-2');
      this.renderer.removeClass(this.el.nativeElement, 'prp-animate-top');
      this.renderer.removeClass(this.el.nativeElement, 'prp-white');

    }
  }
}
