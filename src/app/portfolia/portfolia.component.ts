import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbModal, NgbActiveModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-prp-portfolia',
  templateUrl: './portfolia.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./portfolia.component.css'],
})
export class PortfoliaComponent implements OnInit {
 closeResult: string;

  ngOnInit() {

  }
  constructor(private modalService: NgbModal) {}

   open(content) {
     this.modalService.open(content).result.then((result) => {
       this.closeResult = `Closed with: ${result}`;
     }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   }

   private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
