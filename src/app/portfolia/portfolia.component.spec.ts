import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfoliaComponent } from './portfolia.component';

describe('PortfoliaComponent', () => {
  let component: PortfoliaComponent;
  let fixture: ComponentFixture<PortfoliaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfoliaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfoliaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
