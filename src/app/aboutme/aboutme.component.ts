import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prp-aboutme',
  templateUrl: './aboutme.component.html',
  styleUrls: ['./aboutme.component.css']
})
export class AboutmeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
     window.scrollTo(0, 0);
  }

}
