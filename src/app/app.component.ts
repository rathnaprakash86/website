import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-prp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit {


 constructor() {}

 ngOnInit() {
      window.scrollTo(0, 0);
 }

}
