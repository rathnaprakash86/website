import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';



import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AboutmeComponent } from './aboutme/aboutme.component';
import { SkillsComponent } from './skills/skills.component';
import { PortfoliaComponent } from './portfolia/portfolia.component';
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './footer/footer.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NavigationStyleDirective } from './navigation/navigation-style.directive';
import { NavigationOpenerDirective } from './navigation/navigation-opener.directive';
import { NavigationscrollDirective } from './navigation/navigation-scroll.directive';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { EmailService } from './contact/emailService';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    AboutmeComponent,
    SkillsComponent,
    PortfoliaComponent,
    ContactComponent,
    FooterComponent,
    NavigationComponent,
    NavigationStyleDirective,
    NavigationOpenerDirective,
    NavigationscrollDirective,
    PagenotfoundComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDY4shVBG1ikrXPwf7i6kPpBYOSfeS1_eM'
    }),
  ],
  providers: [EmailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
